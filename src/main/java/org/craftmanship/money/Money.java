package org.craftmanship.money;

import java.math.BigDecimal;

public interface Money {

    BigDecimal getValue();

    String getCurrency();

    Money add(Money money);
}
