package org.craftmanship.money;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;

public class BigDecimalMoney implements Money {

    private final BigDecimal value;
    private final String currency;

    private BigDecimalMoney(BigDecimal value, String currency) {
        this.value = value;
        this.currency = currency;
    }

    public static Money of(String money) {
        BigDecimal value = valueOf(
                Double.valueOf(
                        money.substring(0, money.length() - 3)));
        String currency = money.substring(money.length() - 3);
        return new BigDecimalMoney(value, currency);
    }

    @Override
    public BigDecimal getValue() {
        return value;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public Money add(Money other) {
        return new BigDecimalMoney(
                this.getValue().add(other.getValue()),
                this.getCurrency());
    }

    @Override
    public String toString() {
        return value + currency;
    }
}
