[.opaque]
== https://gitlab.com/ibon-urrutia-presentations/artesania_software/merge_requests/3[Test falla!]
[source,bash]
----
Condition not satisfied:

result.toString() == expected
|      |          |  |
0.0EUR 0.0EUR     |  0.00EUR
                  false
                  1 difference (85% similarity)
                  0.0(-)EUR
                  0.0(0)EUR
----

image::milton.jpg[background, size=cover]

[.opaque]
=== Segundo bug?
[source,bash]
----
$ ./bin/addMoney 1.546,67EUR 7.943,28EUR
Exception in thread "main" java.lang.NumberFormatException:
For input string: "1.546,67"
at sun.misc.FloatingDecimal.readJavaFormatString(FloatingDecimal.java:2043)
at sun.misc.FloatingDecimal.parseDouble(FloatingDecimal.java:110)
at java.lang.Double.parseDouble(Double.java:538)
at java.lang.Double.valueOf(Double.java:502)
at org.craftmanship.money.BigDecimalMoney.of(BigDecimalMoney.java:18)
at org.craftmanship.money.AddMoney.main(AddMoney.java:8)
----
https://es.wikipedia.org/wiki/Separador_decimal[Separador decimal]

image::milton.jpg[background, size=cover]

[.opaque]
=== Tercer y cuarto bug?
[source,bash]
----
$ ./bin/addMoney 1546.67JPY 7943.28JPY
9489.95JPY
$ ./bin/addMoney 1546.67KWD 7943.28KWD
9489.95KWD
----

image::milton.jpg[background, size=cover]

[.opaque]
=== https://es.wikipedia.org/wiki/ISO_4217[ISO 4217]
En Java aún https://jcp.org/aboutJava/communityprocess/final/jsr354/index.html[no se sabe]
 cuando se va a incluir la https://www.baeldung.com/java-money-and-currency[implementación]

image::engineer.jpg[background, size=auto 100%]

[.opaque]
=== `Money` is not Money
image::the-treachery-of-images.jpg[background, size=auto 100%]

[.opaque]
=== El Software es una hipótesis
image::fractal.jpg[background, size=cover]

[.opaque]
=== El mundo está REPLETO de requisitos no funcionales!
image::cafeteras.jpg[background, size=cover]

[.opaque]
=== Calidad = Tiempo planeado + funcionalidades requeridas
image::fractal.jpg[background, size=cover]

[.opaque]
=== Usar soluciones probadas de diseño para problemas similares
{design_patterns}
{refactoring}

image::tools.png[background, size=cover]

[.opaque]
=== Entender el contexto del sofware para diseñarlo efectivamente
{domain_driven_design}

image::tools.png[background, size=cover]

[.opaque]
=== Hacer https://gitlab.com/ibon-urrutia-presentations/artesania_software/blob/master/src/test/groovy/org/craftmanship/money/MoneySpec.groovy[explícitas nuestras hipótesis] y comprobarlas
{test_driven_development}
{evolutionary_design}

image::tools.png[background, size=cover]

