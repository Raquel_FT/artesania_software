= Artesanía del software
Ibon Urrutia <urrutia.ibon@gmail.com>
20018-11-29
:revnumber: ${project-version}
ifndef::imagesdir[:imagesdir: images]
:source-highlighter: highlightjs
:customcss: css/custom.css
:software_craftmanship: http://thecleancoder.blogspot.com/2011/01/software-craftsmanship-what-it-all.html[#software_craftmanship]
:clean_code: https://cvuorinen.net/2014/04/what-is-clean-code-and-why-should-you-care/[#clean_code]
:version_control: https://www.atlassian.com/git/tutorials/what-is-version-control[#version_control]
:build_automation: https://en.wikipedia.org/wiki/Build_automation[#build_automation]
:test_automation: https://en.wikipedia.org/wiki/Test_automation[#test_automation]
:continous_integration: https://martinfowler.com/articles/continuousIntegration.html[#continous_integration]
:semantic_versioning: https://semver.org/[#semantic_versioning]
:dependency_management: https://docs.gradle.org/current/userguide/dependency_management_terminology.html[#dependency_management]
:artifact_repository: https://blog.sonatype.com/2009/04/what-is-a-repository/[#artifact_repository]
:design_patterns: https://sourcemaking.com/design_patterns[#design_patterns]
:refactoring: https://refactoring.com/[#refactoring]
:domain_driven_design: https://medium.com/the-coding-matrix/ddd-101-the-5-minute-tour-7a3037cf53b8[#domain_driven_design]
:test_driven_development: https://blog.scottlogic.com/2018/08/24/is-test-driven-development-right-for-you.html[#test_driven_development]
:evolutionary_design: https://mozaicworks.com/blog/what-is-evolutionary-design/[#evolutionary_design]
:scrum: https://www.youtube.com/watch?v=2Vt7Ik8Ublw[#scrum]
:kanban: https://www.atlassian.com/agile/kanban[#kanban]
:behaviour_driven_development: https://medium.com/@TechMagic/get-started-with-behavior-driven-development-ecdca40e827b[#bdd]
:agile: https://agilemanifesto.org/[#agile]
:devops: https://theagileadmin.com/what-is-devops/[#devops]
:infrastructure_as_code: https://en.wikipedia.org/wiki/Infrastructure_as_code[#infrastructure_as_code]
:continous_delivery: https://www.atlassian.com/continuous-delivery/ci-vs-ci-vs-cd[#continous_delivery]
:site_reliability_engineering: https://landing.google.com/sre/sre-book/toc/index.html[#site_reliability_engineering]
:bizdevops: https://stackify.com/bizdevops-guide/[#bizdevops]

[.opaque]
== Quien soy yo?
[%step]
* Ingeniero de telecomunicaciones
* Más de 15 años ganándome la vida haciendo software
* Dos veces _emprendedor_ fracasado
* Desde hace 6 años vivo en Suiza

image::anton.jpg[background, size=auto 100%, role=right]

[.opaque]
=== Artesanía del software revisited
image::tools.png[background, size=cover]

https://gitlab.com/ibon-urrutia-presentations/artesania_software

{software_craftmanship}


include::que_es_software.adoc[]

include::que_es_desarrollo_software.adoc[]

include::caso_practico_sumar_dinero.adoc[]

include::version_1.0.0.adoc[]

include::version_2.0.0.adoc[]

include::version_2.1.0.adoc[]

include::version_3.0.0.adoc[]

include::version_4.0.0.adoc[]

include::devops.adoc[]

include::conclusion.adoc[]




